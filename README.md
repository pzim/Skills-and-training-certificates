# Skills-and-training-certificates
All my skills and training certificates :
- [DATA developer](https://gitlab.com/pzim/Skills-and-training-certificates/tree/master/DATA%20developer) / [See my DATA projects :blue_book:](https://gitlab.com/pzim//DATA-developer)
- [Web developer](https://gitlab.com/pzim//Skills-and-training-certificates/tree/master/Web%20developer)
- [Project management](https://gitlab.com/pzim/Skills-and-training-certificates/tree/master/Project%20management)
- [Python developer](https://gitlab.com/pzim/Skills-and-training-certificates/tree/master/Python%20developer) / [See my Python projects :blue_book:](https://gitlab.com/pzim//DATA-developer)
[and other little Python scripts for Linux :blue_book:](https://gitlab.com/pzim/Tools-for-Linux)

- [Other skills](https://gitlab.com/pzim/Skills-and-training-certificates/tree/master/Other%20certificates)

See my profils :


- [Openclassrooms](https://gitlab.com/pzim//Skills-and-training-certificates/blob/master/Tableau%20de%20bord%20-%20OpenClassrooms.png)

- [Codecademy](https://www.codecademy.com/profiles/Pzim)

- [Sololearn](https://www.sololearn.com/Profile/13232439)



------------------------------------------------

![Alt-Text](DATA developer/Attestation de fin de formation DDN.png)


------------------------------------------------


![Alt-Text](DATA%20developer/DEVELOPPER%20UNE%20BASE%20DE%20DONNEES.png)


------------------------------------------------


![Alt-Text](DATA%20developer/EXPLOITER%20UNE%20BASE%20DE%20DONNEES.png)

------------------------------------------------

![Alt-Text](Project%20management/M%C3%A9thodes%20agiles%20de%20gestion%20et%20amor%C3%A7age%20de%20projet%20.png)

------------------------------------------------

![Alt-Text](Python%20developer/Python%20Sololearn.png)

